﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SwimmingPool.DomainObjects
{
    public class SwimmingPool : DomainObject
    {
        public string Address { get; set; }

        public string WebSite { get; set; }

        public string SwimmingPoolName { get; set; }

        public string SportZoneName { get; set; }
    }
}
