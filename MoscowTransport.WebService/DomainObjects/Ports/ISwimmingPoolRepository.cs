﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace SwimmingPool.DomainObjects.Ports
{
    public interface IReadOnlySwimmingPoolRepository
    {
        Task<SwimmingPool> GetSwimmingPool(long id);

        Task<IEnumerable<SwimmingPool>> GetAllSwimmingPool();

        Task<IEnumerable<SwimmingPool>> QuerySwimmingPool(ICriteria<SwimmingPool> criteria);

    }

    public interface ISwimmingPoolRepository
    {
        Task AddSwimmingPool(SwimmingPool swimmingPool);

        Task RemoveSwimmingPool(SwimmingPool swimmingPool);

        Task UpdateSwimmingPool(SwimmingPool swimmingPool);
    }
}
