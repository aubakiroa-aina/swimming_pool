﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SwimmingPool.DomainObjects;

namespace SwimmingPool.WebService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RoutesController : ControllerBase
    {
        private readonly ILogger<RoutesController> _logger;

        public RoutesController(ILogger<RoutesController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<DomainObjects.SwimmingPool> GetRoute()
        {
            return new List<DomainObjects.SwimmingPool>() 
            { 
                new DomainObjects.SwimmingPool() 
                { 
                    Id = 1,
                    Address = "748",
                    WebSite = "Ст. Ховрино - Ст. МЦД Бескудниково",
                    /*Type = TransportType.Bus,
                    Organization = new TransportOrganization()
                    {
                        Id = 1,
                        Name = "ГУП \"Мосгортранс\"",
                        TimeZone = "Europe/Moscow",
                        WebSite = "http://mosgortrans.ru"
                    }*/
                } 
            };
        }
    }
}
